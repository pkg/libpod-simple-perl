libpod-simple-perl (3.42-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 19:26:32 +0000

libpod-simple-perl (3.42-1) unstable; urgency=medium

  * Import upstream version 3.42.
  * Declare compliance with Debian Policy 4.5.1.

 -- gregor herrmann <gregoa@debian.org>  Wed, 18 Nov 2020 21:26:52 +0100

libpod-simple-perl (3.41-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Wrap long lines in changelog entries: 3.02-1.
  * Update standards version to 4.5.0, no changes needed.

  [ gregor herrmann ]
  * Import upstream version 3.41.
  * Update years of packaging copyright.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Mon, 12 Oct 2020 21:11:53 +0200

libpod-simple-perl (3.40-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 3.40.
  * Update years of packaging copyright.
  * debian/copyright: add info about new file.
  * Annotate test-only build dependencies with <!nocheck>.
  * Declare compliance with Debian Policy 4.4.1.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.
  * Remove obsolete fields Name, Contact from debian/upstream/metadata.

 -- gregor herrmann <gregoa@debian.org>  Sat, 02 Nov 2019 03:55:11 +0100

libpod-simple-perl (3.35-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sat, 20 Feb 2021 01:27:59 +0000

libpod-simple-perl (3.35-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.
  * Remove Jay Bonci from Uploaders. Thanks for your work!
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Lucas Kanashiro ]
  * Import upstream version 3.35
  * Declare compliance with Debian policy 3.9.8

 -- Lucas Kanashiro <kanashiro@debian.org>  Thu, 22 Dec 2016 15:31:50 -0200

libpod-simple-perl (3.32-1) unstable; urgency=medium

  * Team upload.

  [ Lucas Kanashiro ]
  * Import upstream version 3.32
  * Update upstream metadata

  [ gregor herrmann ]
  * Update Upstream-Contact in debian/copyright.

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Mon, 09 Nov 2015 00:11:11 -0200

libpod-simple-perl (3.31-1) unstable; urgency=medium

  * Team upload.

  [ Lucas Kanashiro ]
  * Import upstream version 3.31
  * Bump debhelper compatibility level to 9

  [ gregor herrmann ]
  * Remove unversioned 'perl' from Depends.

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Thu, 01 Oct 2015 14:18:28 -0300

libpod-simple-perl (3.30-1) unstable; urgency=medium

  * Team upload

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Damyan Ivanov ]
  * Add debian/upstream/metadata
  * Import upstream version 3.30
    Closes: #788268 -- uninstallable with perl 5.22
  * Add Testsuite control entry
  * drop spelling.patch, applied upstream
  * drop inc/Module/* in d/copyright, there's no inc/ anymore
  * Declare conformance with Policy 3.9.6

 -- Damyan Ivanov <dmn@debian.org>  Fri, 12 Jun 2015 11:39:53 +0000

libpod-simple-perl (3.28-1) unstable; urgency=low

  * Imported Upstream version 3.28.
  * Email change: Florian Schlichting -> fsfs@debian.org.
  * Bumped Standards-Version to 3.9.4 (no change).
  * Bumped years of packaging copyright.
  * Added spelling.patch fixing typos in POD.

 -- Florian Schlichting <fsfs@debian.org>  Thu, 08 Aug 2013 18:40:46 +0200

libpod-simple-perl (3.26-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Dominic Hargreaves ]
  * New upstream release

 -- Dominic Hargreaves <dom@earth.li>  Sun, 14 Apr 2013 12:45:36 +0100

libpod-simple-perl (3.22-1) unstable; urgency=low

  * New upstream release.
  * Remove patches, all applied upstream.
  * Update years of packaging copyright.
  * Add debian/NEWS mentioning a compatibility change.

 -- gregor herrmann <gregoa@debian.org>  Sat, 02 Jun 2012 19:46:46 +0200

libpod-simple-perl (3.20-1) unstable; urgency=low

  * Imported Upstream version 3.20.
  * Bumped Standards-Version to 3.9.3 (use copyright-format 1.0).
  * Forwarded patches to upstream.
  * Fixed a format violation in debian/copyright (files should be separated by
    whitespace, not comma).
  * Added fix-pod-spelling2.patch.
  * Added myself to uploaders and copyright.

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Sun, 04 Mar 2012 23:02:40 +0100

libpod-simple-perl (3.19-1) unstable; urgency=low

  * New upstream release.
  * Update years of packaging copyright.
  * Add a new patch to fix POD syntax errors,

 -- gregor herrmann <gregoa@debian.org>  Sun, 28 Aug 2011 20:23:42 +0200

libpod-simple-perl (3.18-1) unstable; urgency=low

  * Team upload.

  [ Ansgar Burchardt ]
  * New upstream release.
  * debian/control: Convert Vcs-* fields to Git.
  * debian/watch: Disallow "-" in upstream versions.
  * Bump Standards-Version to 3.9.2 (no changes).

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 21 Aug 2011 23:44:53 +0200

libpod-simple-perl (3.16-1) unstable; urgency=low

  * New upstream release
  * Rewrite control description
  * Bump to debhelper compat 8
  * Refresh and remove unneeded patches

 -- Jonathan Yu <jawnsy@cpan.org>  Wed, 16 Mar 2011 20:13:03 -0400

libpod-simple-perl (3.15-1) unstable; urgency=low

  [ Wen Heping ]
  * New upstream release
  * Standards-Version 3.9.1 (no changes)

  [ gregor herrmann ]
  * debian/rules: remove override, the "offending" files we deleted are not
    shipped anymore.
  * Add /me to Uploaders.
  * Make perl build dependency unversioned (stable has already 5.10).
  * Refresh patches, add one more spelling fix.
  * debian/copyright: update license stanzas and list of copyright holders for
    debian/*.

 -- gregor herrmann <gregoa@debian.org>  Tue, 21 Dec 2010 15:46:47 +0100

libpod-simple-perl (3.14-1) unstable; urgency=low

  * New upstream release
  * Use new 3.0 (quilt) source format
  * Standards-Version 3.8.4 (no changes)
  * Add a patch to fix POD spelling errors
  * Update copyright information to DEP5 format

 -- Jonathan Yu <jawnsy@cpan.org>  Sat, 01 May 2010 23:28:32 -0400

libpod-simple-perl (3.13-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release

  [ gregor herrmann ]
  * Add a patch to fix the POD and a test; add quilt framework.

 -- Jonathan Yu <jawnsy@cpan.org>  Thu, 17 Dec 2009 11:04:50 -0500

libpod-simple-perl (3.11-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release

  [ gregor herrmann ]
  * debian/copyright: remove stanza for an extra file that is now covered by
    the general clause.

 -- Jonathan Yu <jawnsy@cpan.org>  Wed, 09 Dec 2009 18:04:23 -0500

libpod-simple-perl (3.10-1) unstable; urgency=low

  * New upstream release

 -- Jonathan Yu <jawnsy@cpan.org>  Thu, 12 Nov 2009 19:34:31 -0500

libpod-simple-perl (3.09-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Standards-Version 3.8.3 (no changes)

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ Ryan Niebur ]
  * Update jawnsy's email address

  [ gregor herrmann ]
  * debian/control: Changed: (build-)depend on perl instead of perl-
    modules.
  * debian/watch: use extended regexp for matching upstream versions.
  * debian/rules: minimize and use override_* feature.

 -- Jonathan Yu <jawnsy@cpan.org>  Mon, 26 Oct 2009 19:03:16 -0400

libpod-simple-perl (3.08-1) unstable; urgency=low

  * New upstream release
    + Fix encoding handling for code in paragraphs (RT#45829)
    + Now installs in perl directory.. need to see if install is broken
  * Standards-Version 3.8.2
  * Fixed copyright (use info from changelog for debian/)
  * Added libhtml-parser-perl (for HTML::Entities) to B-D-I for testing

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

 -- Jonathan Yu <frequency@cpan.org>  Fri, 17 Jul 2009 09:17:08 -0400

libpod-simple-perl (3.07-1) unstable; urgency=low

  * New upstream release
    Closes: #481958 -- contains the same version that's in perl-modules
  * add perl-modules (>= 5.10) to B-D-I as Test 1.25 is required by tests
  * add upstream maintainers info to debian/copyright
  * replace debian/rules with a three-liner thanks to debhelper 7
  * remove old perlpod* manages and POD, overriding the PODs in perl-doc
    Closes: #481961 -- perlpod and perlpodspec are also in the core

 -- Damyan Ivanov <dmn@debian.org>  Mon, 30 Jun 2008 00:01:21 +0300

libpod-simple-perl (3.05-2) unstable; urgency=low

  [ gregor herrmann ]
  * debian/rules: delete /usr/lib/perl5 only if it exists
    closes: #458143 -- FTBFS with perl from experimental

  [ Damyan Ivanov ]
  * add v? to watchfile pattern
  * add myself to Uploaders
  * debhelper compatibility level 6

 -- Damyan Ivanov <dmn@debian.org>  Tue, 15 Jan 2008 16:23:35 +0200

libpod-simple-perl (3.05-1) unstable; urgency=low

  [ Gregor Herrmann ]

  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza).
  * Set Maintainer to Debian Perl Group.
  * Use dist-based URL in debian/watch.


  [ Rene Mayorga ]

  * New upstream release
  * Move debhelper to Build-Depends instead B-D-I
  * Bump Standard version to 3.7.2
     + Set compat level to 5
  * Add ${misc:Depends} to Depends
  * debian/rules
     + use $(PERL) instead perl
     + add build/install-stamp targets
     + Move 'build' and 'test' to build target
     + de-ignoring output from make clean
     + remove un-used dh_* calls
     + remove un-useful README from debian package
  * debian/copyright
     + refresh copyright info
     + Add copyright info for the Debian Packaging

  [ Gunnar Wolf ]
  * Bumped up standards-version to 3.7.3  (No changes needed)
  * Added myself as uploader

 -- Gunnar Wolf <gwolf@debian.org>  Fri, 21 Dec 2007 21:10:27 -0600

libpod-simple-perl (3.04-1) unstable; urgency=low

  * New upstream release (Closes: #349270)
  * Standards-version bump (no other changes)
  * Fixed up watch file to use uupdate
  * Cleaned t/ directory buildup

 -- Jay Bonci <jaybonci@debian.org>  Fri, 24 Feb 2006 15:00:45 -0500

libpod-simple-perl (3.02-2) unstable; urgency=low

  * Fix FTBFS b/c test suite (Closes: #277960)
    - Adds build-dep on perl and perl-doc

 -- Jay Bonci <jaybonci@debian.org>  Wed, 27 Oct 2004 15:59:50 -0400

libpod-simple-perl (3.02-1) unstable; urgency=low

  * New upstream version
  * Adds watchfile (slightly more complicated to weed out a bad ver. number on
    CPAN)

 -- Jay Bonci <jaybonci@debian.org>  Wed,  6 Oct 2004 02:40:08 -0400

libpod-simple-perl (2.05-1) unstable; urgency=low

  * New upstream version

 -- Jay Bonci <jaybonci@debian.org>  Fri,  2 Jan 2004 15:38:21 -0500

libpod-simple-perl (2.03-1) unstable; urgency=low

  * Initial Release (Closes: #184046)

 -- Jay Bonci <jaybonci@debian.org>  Wed, 24 Sep 2003 00:09:23 -0400
